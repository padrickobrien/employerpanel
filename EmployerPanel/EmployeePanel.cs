﻿using NetPanelLib;
using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace EmployerPanel
{
    public partial class EmployeePanel : UserControl , IPanelControl
    {
        private IPanelHost host = null;
        private int fileNumber;
        private int empnumber;
        List<ReliantEmployee> employees; 

        public EmployeePanel()
        {
            InitializeComponent();
        }

        #region IPanelControl Members

        public void Clear()
        {
            InitControls();
        }

        public string ControlData
        {
            set {}
        }

        public void DisplayAccount(int accountNumber)
        {
            string name;
            ResetForm();
            lbxDebtors.Items.Clear();
            fileNumber = accountNumber;
            
            SqlConnection con =  new SqlConnection(GSSI.LatitudeLogin.Latitude.ConnectionString.ToString());

            employees = GetEmployees(fileNumber);
            //populate listbox with debtors. This allows to switch between debtors.

            foreach (var emp in employees)
            {
                name = emp.EmployeeID.ToString() + "-" + emp.FirstName + " " + emp.LastName;
                lbxDebtors.Items.Add(name);
            }
        }

        public void Initialize(string connectionString, int userID)
        {
            //throw new NotImplementedException();
        }

        public string MessageReceived(string command, string data)
        {
            throw new NotImplementedException();
        }

        public IPanelHost PanelHost
        {
            set { host = value; }
        }

        public string PanelName
        {
            get { return "Employee Panel"; }
        }

        private void InitControls()
        {
            // Clear the Tab Pages
            this.firstNameTextBox.Text.ToString();
        }

        private void lbxDebtors_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetForm();
            string selectedDebtor = lbxDebtors.SelectedItem.ToString();
            lblDebtorID.Text = selectedDebtor.IndexOf('-') > 0 ? selectedDebtor.Substring(0, selectedDebtor.IndexOf("-")) : "";
            //empnumber = Convert.ToInt32(lblDebtorID.Text);
            if (!Int32.TryParse(lblDebtorID.Text, out empnumber))
            {
                lblEmployeeID.Text = "No Employee Information";
            }
            else
            {
                lblEmployeeID.Text = empnumber.ToString();
                ReliantEmployee currEmployee = employees.Where(x => x.EmployeeID == empnumber).First();
                UpdateForm(currEmployee);
            }

            ////this will be replaced with a call to a helper method that populates the form
            //firstNameTextBox.Text = currEmployee.FirstName;
            //lastNameTextBox.Text = currEmployee.LastName;
            //positionTitleTextBox.Text = currEmployee.PositionTitle;

            ////the below will be replaced as well, leaving it in at the moment as scaffolding
            //SqlConnection con = new SqlConnection(GSSI.LatitudeLogin.Latitude.ConnectionString.ToString());

            //if (empnumber > 0)
            //{
            //    lblEmployeeID.Text = empnumber.ToString();

            //    SqlParameter employeeid = new SqlParameter("@EmployeeID", SqlDbType.Int)
            //    {
            //        Value = lblEmployeeID.Text
            //    };
            //    employeeid.Value = lblEmployeeID.Text;

            //    SqlCommand getemployerinfoCOM = new SqlCommand
            //    {
            //        CommandType = CommandType.StoredProcedure,
            //        CommandText = "Custom_GetEmployers",
            //        Connection = con
            //    };
            //    getemployerinfoCOM.Parameters.Add(employeeid);

            //    con.Open();
            //    using (SqlDataReader getemployerinforeader = getemployerinfoCOM.ExecuteReader())
            //    {
            //        while (getemployerinforeader.Read())
            //        {
            //            employerNameTextBox.Text = getemployerinforeader["EmployerName"].ToString();
            //            employerStateTextBox.Text = getemployerinforeader["EmployerState"].ToString();
            //            employerStreet1TextBox.Text = getemployerinforeader["EmployerStreet1"].ToString();
            //            employerStreet2TextBox.Text = getemployerinforeader["EmployerStreet2"].ToString();
            //        }
            //    }
            //}
            //else 
            //{
            //    lblEmployeeID.Text = "No Employee Information";
            //}
            //con.Close();
        }

        private void Update_Button_Click(object sender, EventArgs e)
        {
            int debtorid = Convert.ToInt32(lblDebtorID.Text);

            UpdateEmployee(debtorid);
            //using (SqlConnection con = new SqlConnection(GSSI.LatitudeLogin.Latitude.ConnectionString.ToString()))
            //{
            //    int debtorid = Convert.ToInt32(lblDebtorID.Text);

            //    SqlCommand updateCOM = new SqlCommand("Custom_EditEmployee", con)
            //    {
            //        CommandType = CommandType.StoredProcedure
            //    };
            //    //updateCOM.CommandType = CommandType.StoredProcedure;
            //    updateCOM.Parameters.Add(new SqlParameter("@PositionTitle", SqlDbType.VarChar, 75));
            //    updateCOM.Parameters.Add(new SqlParameter("@Debtorid", SqlDbType.Int, 20));

            //    //Open connections as late as possible and close as early as possible.
            //    con.Open();
            //    updateCOM.ExecuteNonQuery();
            //    con.Close();
            //}
        }

        #region helper methods
        //Resets the form to default values.
        private void ResetForm()
        {
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            positionTitleTextBox.Text = "";
            employerNameTextBox.Text = "";
            employerStateTextBox.Text = "";
            employerStreet1TextBox.Text = "";
            employerStreet2TextBox.Text = "";
            lblDebtorID.Text = "Debtorid";
            lblEmployeeID.Text = "Employee Number";
            empnumber = 0;
        }

        private void UpdateForm(ReliantEmployee emp)
        {
            //this line to be removed once we get employer tabbing in place
            ReliantEmployer currEmployer = emp.Employers.First();

            firstNameTextBox.Text = emp.FirstName;
            lastNameTextBox.Text = emp.LastName;
            positionTitleTextBox.Text = emp.PositionTitle;
            employerNameTextBox.Text = currEmployer.EmployerName;
            employerStateTextBox.Text = currEmployer.EmployerState;
            employerStreet1TextBox.Text = currEmployer.EmployerStreet1;
            employerStreet2TextBox.Text = currEmployer.EmployerStreet2;
        }

        private List<ReliantEmployee> GetEmployees(int fileNumber)
        {
            var employees = new List<ReliantEmployee>();
            ReliantEmployee temp;
            using (var sqlConn = new SqlConnection(GSSI.LatitudeLogin.Latitude.ConnectionString))
            {
                var command = new SqlCommand("Custom_GetEmployees", sqlConn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add(new SqlParameter("@FileNumber", SqlDbType.Int) { Value = fileNumber });

                sqlConn.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        temp = new ReliantEmployee()
                        {
                            EmployeeID = Convert.ToInt32(reader["EmployeeID"].ToString()),
                            SSN = reader["SSN"].ToString(),
                            MatchedEmployee = reader["MatchedEmployee"].ToString(),
                            MatchedActiveRecord = reader["MatchedActiveRecord"].ToString(),
                            PayrollDisclaimer = reader["PayrollDisclaimer"].ToString(),
                            AlternateID = reader["AlternateID"].ToString(),
                            EmployeeSSN = reader["EmployeeSSN"].ToString(),
                            FirstName = reader["FirstName"].ToString(),
                            MiddleName = reader["MiddleName"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            PositionTitle = reader["PositionTitle"].ToString(),
                            DivisionCode = reader["DivisionCode"].ToString(),
                            StatusCode = reader["StatusCode"].ToString(),
                            StatusMessage = reader["StatusMessage"].ToString(),
                            StatusType = reader["StatusType"].ToString(),
                            InfoEffectiveDate = Convert.ToDateTime(reader["InfoEffectiveDate"].ToString()),
                            MostRecentHireDate = Convert.ToDateTime(reader["MostRecentHireDate"].ToString()),
                            TotalLengthOfSvc = Convert.ToInt32(reader["TotalLengthOfSvc"].ToString()),
                            TerminationDate = Convert.ToDateTime(reader["TerminationDate"].ToString()),
                            ReferenceNumber = Convert.ToInt32(reader["ReferenceNumber"].ToString()),
                            TrackingNumber = Convert.ToInt32(reader["TrackingNumber"].ToString()),
                            FraudAlert = reader["FraudAlert"].ToString(),
                            MilitaryAlert = reader["MilitaryAlert"].ToString(),
                            DisputeMessage = reader["DisputeMessage"].ToString()
                        };
                        employees.Add(temp);
                    }
                }
                sqlConn.Close();
            }
            return employees;
        }

        private void UpdateEmployee(int employeeID)
        {
            var employee = employees.Where(x => x.EmployeeID == employeeID).First();

            var cmd = new SqlCommand()
            {
                CommandText = "Custom_EditEmployee",
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.Add("@EmployeeID", SqlDbType.Int);
            cmd.Parameters.Add("@SSN", SqlDbType.VarChar, 15);
            cmd.Parameters.Add("@MatchedEmployee", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@MatchedActiveRecord", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@PayrollDisclaimer", SqlDbType.VarChar, 425);
            cmd.Parameters.Add("@AlternateID", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@EmployeeSSN", SqlDbType.VarChar, 15);
            cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@MiddleName", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@PositionTitle", SqlDbType.VarChar, 75);
            cmd.Parameters.Add("@DivisionCode", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@StatusCode", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@StatusMessage", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@StatusType", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@InfoEffectiveDate", SqlDbType.Date);
            cmd.Parameters.Add("@MostRecentHireDate", SqlDbType.Date);
            cmd.Parameters.Add("@TotalLengthOfSvc", SqlDbType.Int);
            cmd.Parameters.Add("@TerminationDate", SqlDbType.Date);
            cmd.Parameters.Add("@ReferenceNumber", SqlDbType.Int);
            cmd.Parameters.Add("@TrackingNumber", SqlDbType.Int);
            cmd.Parameters.Add("@FraudAlert", SqlDbType.VarChar, 100);
            cmd.Parameters.Add("@MilitaryAlert", SqlDbType.VarChar, 100);
            cmd.Parameters.Add("@DisputeMessage", SqlDbType.VarChar, 100);

            using (var sqlconn = new SqlConnection(GSSI.LatitudeLogin.Latitude.ConnectionString))
            {
                cmd.Connection = sqlconn;
                sqlconn.Open();
                cmd.ExecuteNonQuery();
                sqlconn.Close();
            }
        }



        #endregion

        #endregion
    }
}
