﻿namespace EmployerPanel
{
    partial class EmployeePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label firstNameLabel;
            System.Windows.Forms.Label lastNameLabel;
            System.Windows.Forms.Label positionTitleLabel;
            System.Windows.Forms.Label employerNameLabel;
            System.Windows.Forms.Label employerStateLabel;
            System.Windows.Forms.Label employerStreet1Label;
            System.Windows.Forms.Label employerStreet2Label;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeePanel));
            this.custom_ReliantEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.custom_ReliantEmployeeBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.custom_ReliantEmployeeBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.positionTitleTextBox = new System.Windows.Forms.TextBox();
            this.lblDebtorID = new System.Windows.Forms.Label();
            this.lbxDebtors = new System.Windows.Forms.ListBox();
            this.custom_ReliantEmployee_EmployerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.employerNameTextBox = new System.Windows.Forms.TextBox();
            this.employerStateTextBox = new System.Windows.Forms.TextBox();
            this.employerStreet1TextBox = new System.Windows.Forms.TextBox();
            this.employerStreet2TextBox = new System.Windows.Forms.TextBox();
            this.Update_Button = new System.Windows.Forms.Button();
            this.lblEmployeeID = new System.Windows.Forms.Label();
            firstNameLabel = new System.Windows.Forms.Label();
            lastNameLabel = new System.Windows.Forms.Label();
            positionTitleLabel = new System.Windows.Forms.Label();
            employerNameLabel = new System.Windows.Forms.Label();
            employerStateLabel = new System.Windows.Forms.Label();
            employerStreet1Label = new System.Windows.Forms.Label();
            employerStreet2Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.custom_ReliantEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.custom_ReliantEmployeeBindingNavigator)).BeginInit();
            this.custom_ReliantEmployeeBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.custom_ReliantEmployee_EmployerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // firstNameLabel
            // 
            firstNameLabel.AutoSize = true;
            firstNameLabel.Location = new System.Drawing.Point(135, 54);
            firstNameLabel.Name = "firstNameLabel";
            firstNameLabel.Size = new System.Drawing.Size(60, 13);
            firstNameLabel.TabIndex = 1;
            firstNameLabel.Text = "First Name:";
            // 
            // lastNameLabel
            // 
            lastNameLabel.AutoSize = true;
            lastNameLabel.Location = new System.Drawing.Point(134, 80);
            lastNameLabel.Name = "lastNameLabel";
            lastNameLabel.Size = new System.Drawing.Size(61, 13);
            lastNameLabel.TabIndex = 3;
            lastNameLabel.Text = "Last Name:";
            // 
            // positionTitleLabel
            // 
            positionTitleLabel.AutoSize = true;
            positionTitleLabel.Location = new System.Drawing.Point(135, 106);
            positionTitleLabel.Name = "positionTitleLabel";
            positionTitleLabel.Size = new System.Drawing.Size(70, 13);
            positionTitleLabel.TabIndex = 5;
            positionTitleLabel.Text = "Position Title:";
            // 
            // employerNameLabel
            // 
            employerNameLabel.AutoSize = true;
            employerNameLabel.Location = new System.Drawing.Point(135, 132);
            employerNameLabel.Name = "employerNameLabel";
            employerNameLabel.Size = new System.Drawing.Size(84, 13);
            employerNameLabel.TabIndex = 9;
            employerNameLabel.Text = "Employer Name:";
            // 
            // employerStateLabel
            // 
            employerStateLabel.AutoSize = true;
            employerStateLabel.Location = new System.Drawing.Point(134, 160);
            employerStateLabel.Name = "employerStateLabel";
            employerStateLabel.Size = new System.Drawing.Size(81, 13);
            employerStateLabel.TabIndex = 11;
            employerStateLabel.Text = "Employer State:";
            // 
            // employerStreet1Label
            // 
            employerStreet1Label.AutoSize = true;
            employerStreet1Label.Location = new System.Drawing.Point(135, 186);
            employerStreet1Label.Name = "employerStreet1Label";
            employerStreet1Label.Size = new System.Drawing.Size(90, 13);
            employerStreet1Label.TabIndex = 13;
            employerStreet1Label.Text = "Employer Street1:";
            // 
            // employerStreet2Label
            // 
            employerStreet2Label.AutoSize = true;
            employerStreet2Label.Location = new System.Drawing.Point(134, 212);
            employerStreet2Label.Name = "employerStreet2Label";
            employerStreet2Label.Size = new System.Drawing.Size(90, 13);
            employerStreet2Label.TabIndex = 15;
            employerStreet2Label.Text = "Employer Street2:";
            // 
            // custom_ReliantEmployeeBindingSource
            // 
            this.custom_ReliantEmployeeBindingSource.DataSource = typeof(EmployerPanel.Custom_ReliantEmployee);
            // 
            // custom_ReliantEmployeeBindingNavigator
            // 
            this.custom_ReliantEmployeeBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.custom_ReliantEmployeeBindingNavigator.BindingSource = this.custom_ReliantEmployeeBindingSource;
            this.custom_ReliantEmployeeBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.custom_ReliantEmployeeBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.custom_ReliantEmployeeBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.custom_ReliantEmployeeBindingNavigatorSaveItem});
            this.custom_ReliantEmployeeBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.custom_ReliantEmployeeBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.custom_ReliantEmployeeBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.custom_ReliantEmployeeBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.custom_ReliantEmployeeBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.custom_ReliantEmployeeBindingNavigator.Name = "custom_ReliantEmployeeBindingNavigator";
            this.custom_ReliantEmployeeBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.custom_ReliantEmployeeBindingNavigator.Size = new System.Drawing.Size(529, 25);
            this.custom_ReliantEmployeeBindingNavigator.TabIndex = 0;
            this.custom_ReliantEmployeeBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // custom_ReliantEmployeeBindingNavigatorSaveItem
            // 
            this.custom_ReliantEmployeeBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.custom_ReliantEmployeeBindingNavigatorSaveItem.Enabled = false;
            this.custom_ReliantEmployeeBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("custom_ReliantEmployeeBindingNavigatorSaveItem.Image")));
            this.custom_ReliantEmployeeBindingNavigatorSaveItem.Name = "custom_ReliantEmployeeBindingNavigatorSaveItem";
            this.custom_ReliantEmployeeBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.custom_ReliantEmployeeBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.custom_ReliantEmployeeBindingSource, "FirstName", true));
            this.firstNameTextBox.Location = new System.Drawing.Point(227, 51);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(158, 20);
            this.firstNameTextBox.TabIndex = 2;
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.custom_ReliantEmployeeBindingSource, "LastName", true));
            this.lastNameTextBox.Location = new System.Drawing.Point(228, 77);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(157, 20);
            this.lastNameTextBox.TabIndex = 4;
            // 
            // positionTitleTextBox
            // 
            this.positionTitleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.custom_ReliantEmployeeBindingSource, "PositionTitle", true));
            this.positionTitleTextBox.Location = new System.Drawing.Point(228, 103);
            this.positionTitleTextBox.Name = "positionTitleTextBox";
            this.positionTitleTextBox.Size = new System.Drawing.Size(157, 20);
            this.positionTitleTextBox.TabIndex = 6;
            // 
            // lblDebtorID
            // 
            this.lblDebtorID.AutoSize = true;
            this.lblDebtorID.Location = new System.Drawing.Point(473, 12);
            this.lblDebtorID.Name = "lblDebtorID";
            this.lblDebtorID.Size = new System.Drawing.Size(53, 13);
            this.lblDebtorID.TabIndex = 7;
            this.lblDebtorID.Text = "Debtor ID";
            // 
            // lbxDebtors
            // 
            this.lbxDebtors.FormattingEnabled = true;
            this.lbxDebtors.Location = new System.Drawing.Point(16, 51);
            this.lbxDebtors.Name = "lbxDebtors";
            this.lbxDebtors.Size = new System.Drawing.Size(116, 173);
            this.lbxDebtors.TabIndex = 8;
            this.lbxDebtors.SelectedIndexChanged += new System.EventHandler(this.lbxDebtors_SelectedIndexChanged);
            // 
            // custom_ReliantEmployee_EmployerBindingSource
            // 
            this.custom_ReliantEmployee_EmployerBindingSource.DataSource = typeof(EmployerPanel.Custom_ReliantEmployee_Employer);
            // 
            // employerNameTextBox
            // 
            this.employerNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.custom_ReliantEmployee_EmployerBindingSource, "EmployerName", true));
            this.employerNameTextBox.Location = new System.Drawing.Point(228, 129);
            this.employerNameTextBox.Name = "employerNameTextBox";
            this.employerNameTextBox.Size = new System.Drawing.Size(157, 20);
            this.employerNameTextBox.TabIndex = 10;
            // 
            // employerStateTextBox
            // 
            this.employerStateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.custom_ReliantEmployee_EmployerBindingSource, "EmployerState", true));
            this.employerStateTextBox.Location = new System.Drawing.Point(228, 157);
            this.employerStateTextBox.Name = "employerStateTextBox";
            this.employerStateTextBox.Size = new System.Drawing.Size(157, 20);
            this.employerStateTextBox.TabIndex = 12;
            // 
            // employerStreet1TextBox
            // 
            this.employerStreet1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.custom_ReliantEmployee_EmployerBindingSource, "EmployerStreet1", true));
            this.employerStreet1TextBox.Location = new System.Drawing.Point(228, 183);
            this.employerStreet1TextBox.Name = "employerStreet1TextBox";
            this.employerStreet1TextBox.Size = new System.Drawing.Size(157, 20);
            this.employerStreet1TextBox.TabIndex = 14;
            // 
            // employerStreet2TextBox
            // 
            this.employerStreet2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.custom_ReliantEmployee_EmployerBindingSource, "EmployerStreet2", true));
            this.employerStreet2TextBox.Location = new System.Drawing.Point(228, 209);
            this.employerStreet2TextBox.Name = "employerStreet2TextBox";
            this.employerStreet2TextBox.Size = new System.Drawing.Size(157, 20);
            this.employerStreet2TextBox.TabIndex = 16;
            // 
            // Update_Button
            // 
            this.Update_Button.Location = new System.Drawing.Point(422, 201);
            this.Update_Button.Name = "Update_Button";
            this.Update_Button.Size = new System.Drawing.Size(75, 23);
            this.Update_Button.TabIndex = 17;
            this.Update_Button.Text = "Update";
            this.Update_Button.UseVisualStyleBackColor = true;
            this.Update_Button.Click += new System.EventHandler(this.Update_Button_Click);
            // 
            // lblEmployeeID
            // 
            this.lblEmployeeID.AutoSize = true;
            this.lblEmployeeID.Location = new System.Drawing.Point(366, 12);
            this.lblEmployeeID.Name = "lblEmployeeID";
            this.lblEmployeeID.Size = new System.Drawing.Size(67, 13);
            this.lblEmployeeID.TabIndex = 18;
            this.lblEmployeeID.Text = "Employee ID";
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblEmployeeID);
            this.Controls.Add(this.Update_Button);
            this.Controls.Add(employerStreet2Label);
            this.Controls.Add(this.employerStreet2TextBox);
            this.Controls.Add(employerStreet1Label);
            this.Controls.Add(this.employerStreet1TextBox);
            this.Controls.Add(employerStateLabel);
            this.Controls.Add(this.employerStateTextBox);
            this.Controls.Add(employerNameLabel);
            this.Controls.Add(this.employerNameTextBox);
            this.Controls.Add(this.lbxDebtors);
            this.Controls.Add(this.positionTitleTextBox);
            this.Controls.Add(lastNameLabel);
            this.Controls.Add(positionTitleLabel);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(firstNameLabel);
            this.Controls.Add(this.lblDebtorID);
            this.Controls.Add(this.custom_ReliantEmployeeBindingNavigator);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(529, 495);
            ((System.ComponentModel.ISupportInitialize)(this.custom_ReliantEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.custom_ReliantEmployeeBindingNavigator)).EndInit();
            this.custom_ReliantEmployeeBindingNavigator.ResumeLayout(false);
            this.custom_ReliantEmployeeBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.custom_ReliantEmployee_EmployerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource custom_ReliantEmployeeBindingSource;
        private System.Windows.Forms.BindingNavigator custom_ReliantEmployeeBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton custom_ReliantEmployeeBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox positionTitleTextBox;
        private System.Windows.Forms.Label lblDebtorID;
        private System.Windows.Forms.ListBox lbxDebtors;
        private System.Windows.Forms.BindingSource custom_ReliantEmployee_EmployerBindingSource;
        private System.Windows.Forms.TextBox employerNameTextBox;
        private System.Windows.Forms.TextBox employerStateTextBox;
        private System.Windows.Forms.TextBox employerStreet1TextBox;
        private System.Windows.Forms.TextBox employerStreet2TextBox;
        private System.Windows.Forms.Button Update_Button;
        private System.Windows.Forms.Label lblEmployeeID;
    }
}
