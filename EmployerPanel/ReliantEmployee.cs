﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace EmployerPanel
{
    public class ReliantEmployee
    {
        #region privatemembers
        private string _fName;
        private string _mName;
        private string _lName;
        private int _empID;
        private string _SSN;
        private string _matchedEmployee;
        private string _matchedActiveRecord;
        private string _payrollDisclaimer;
        private string _altID;
        private string _employeeSSN;
        private string _posTitle;
        private string _divCode;
        private string _statCode;
        private string _statMsg;
        private string _statType;
        private DateTime _effDate;
        private DateTime _recHireDate;
        private int _servLength;
        private DateTime _termDate;
        private int _trackNum;
        private int _refNum;
        private string _fraudAlert;
        private string _milAlert;
        private string _dispMsg;
        private List<ReliantEmployer> _employers = null; //generally unncessary but placed here to get rid of an annoying intellisense warning
        #endregion

        public ReliantEmployee()
        {

        }

        public ReliantEmployee
        (
            int employeeID,
            string SSN,
            string matchedEmployee,
            string matchedActiveRecord,
            string payrollDisclaimer,
            string alternateID,
            string employeeSSN,
            string firstName,
            string lastName,
            string middleName,
            string positionTitle,
            string divisionCode,
            string statusCode,
            string statusMessage,
            string statusType,
            DateTime infoEffectiveDate,
            DateTime mostRecentHireDate,
            int totalLengthOfService,
            DateTime terminationDate,
            int referenceNumber,
            int trackingNumber,
            string fraudAlert,
            string militaryAlert,
            string disputeMessage
        )
        {
            this._empID = employeeID;
            this._SSN = SSN;
            this._matchedEmployee = matchedEmployee;
            this._matchedActiveRecord = matchedActiveRecord;
            this._payrollDisclaimer = payrollDisclaimer;
            this._altID = alternateID;
            this._employeeSSN = employeeSSN;
            this._fName = firstName;
            this._mName = middleName;
            this._lName = lastName;
            this._posTitle = positionTitle;
            this._divCode = divisionCode;
            this._statCode = statusCode;
            this._statMsg = statusMessage;
            this._statType = statusType;
            this._effDate = infoEffectiveDate;
            this._recHireDate = mostRecentHireDate;
            this._servLength = totalLengthOfService;
            this._termDate = terminationDate;
            this._refNum = referenceNumber;
            this._trackNum = trackingNumber;
            this._fraudAlert = fraudAlert;
            this._milAlert = militaryAlert;
            this._dispMsg = disputeMessage;
        }

        #region public_members
        public int EmployeeID
        {
            get { return _empID; }
            set { _empID = value; }
        }
        public string SSN
        {
            get { return _SSN; }
            set { _SSN = value; }
        }
        public string MatchedActiveRecord
        {
            get { return _matchedActiveRecord; }
            set { _matchedActiveRecord = value; }
        }
        public string MatchedEmployee
        {
            get { return _matchedEmployee; }
            set { _matchedEmployee = value; }
        }
        public string PayrollDisclaimer
        {
            get { return _payrollDisclaimer; }
            set { _payrollDisclaimer = value; }
        }
        public string AlternateID
        {
            get { return _altID; }
            set { _altID = value; }
        }
        public string EmployeeSSN
        {
            get { return _employeeSSN; }
            set { _employeeSSN = value; }
        }
        public string FirstName
        {
            get { return _fName; }
            set { _fName = value; }
        }
        public string MiddleName
        {
            get { return _mName; }
            set { _mName = value; }
        }
        public string LastName
        {
            get { return _lName; }
            set { _lName = value; }
        }
        public string PositionTitle
        {
            get { return _posTitle; }
            set { _posTitle = value; }
        }
        public string DivisionCode
        {
            get { return _divCode; }
            set { _divCode = value; }
        }
        public string StatusCode
        {
            get { return _statCode; }
            set { _statCode = value; }
        }
        public string StatusMessage
        {
            get { return _statMsg; }
            set { _statMsg = value; }
        }
        public string StatusType
        {
            get { return _statType; }
            set { _statType = value; }
        }
        public DateTime InfoEffectiveDate
        {
            get { return _effDate; }
            set { _effDate = value; }
        }
        public DateTime MostRecentHireDate
        {
            get { return _recHireDate; }
            set { _recHireDate = value; }
        }
        public int TotalLengthOfSvc
        {
            get { return _servLength; }
            set { _servLength = value; }
        }
        public DateTime TerminationDate
        {
            get { return _termDate; }
            set { _termDate = value; }
        }
        public int ReferenceNumber
        {
            get { return _refNum; }
            set { _refNum = value; }
        }
        public int TrackingNumber
        {
            get { return _trackNum; }
            set { _trackNum = value; }
        }
        public string FraudAlert
        {
            get { return _fraudAlert; }
            set { _fraudAlert = value; }
        }
        public string MilitaryAlert
        {
            get { return _milAlert; }
            set { _milAlert = value; }
        }
        public string DisputeMessage
        {
            get { return _dispMsg; }
            set { _dispMsg = value; }
        }

        public List<ReliantEmployer> Employers
        {
            get
            {
                if (this._employers == null)
                {
                    SqlCommand cmd = new SqlCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "Custom_GetEmployers",
                        Connection = new SqlConnection(GSSI.LatitudeLogin.Latitude.ConnectionString)
                    };
                    cmd.Parameters.Add(new SqlParameter("@EmployeeID", this._empID));

                    cmd.Connection.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            this._employers.Add(new ReliantEmployer
                            {
                                EmployerID = Convert.ToInt32(sdr["EmployerID"].ToString()),
                                EmployeeID = Convert.ToInt32(sdr["EmployeeID"].ToString()),
                                EmployerCode = sdr["EmployerCode"].ToString(),
                                EmployerName = sdr["EmployerName"].ToString(),
                                EmployerStreet1 = sdr["EmployerStreet1"].ToString(),
                                EmployerStreet2 = sdr["EmployerStreet2"].ToString(),
                                EmployerCity = sdr["EmployerCity"].ToString(),
                                EmployerState = sdr["EmployerState"].ToString(),
                                EmployerPostalCode = sdr["EmployerPostalCode"].ToString(),
                                EmployerISOCountryCode = sdr["EmployerISOCountryCode"].ToString(),
                                EmployerDisclaimer = sdr["EmployerDisclaimer"].ToString()
                            });
                        }
                    }
                    cmd.Connection.Close();
                }

                return this._employers;
            }
        }
        #endregion
    }
}
