﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployerPanel
{
    public class ReliantEmployer
    {
        private int employerID;
        private int employeeID;
        private string employerCode;
        private string employerName;
        private string employerStreet1;
        private string employerStreet2;
        private string employerCity;
        private string employerState;
        private string employerPostalCode;
        private string employerISOCountryCode;
        private string employerDisclaimer;

        public ReliantEmployer()
        {

        }

        public int EmployerID
        {
            get
            {
                return this.employerID;
            }
            set
            {
                this.employerID = value;
            }
        }

        public int EmployeeID
        {
            get
            {
                return this.employeeID;
            }
            set
            {
                this.employeeID = value;
            }
        }

        public string EmployerCode
        {
            get
            {
                return this.employerCode;
            }
            set
            {
                this.employerCode = value;
            }
        }

        public string EmployerName
        {
            get
            {
                return this.employerName;
            }
            set
            {
                this.employerName = value;
            }
        }

        public string EmployerStreet1
        {
            get
            {
                return this.employerStreet1;
            }
            set
            {
                this.employerStreet1 = value;
            }
        }

        public string EmployerStreet2
        {
            get
            {
                return this.employerStreet2;
            }
            set
            {
                this.employerStreet2 = value;
            }
        }

        public string EmployerCity
        {
            get
            {
                return this.employerCity;
            }
            set
            {
                this.employerCity = value;
            }
        }

        public string EmployerState
        {
            get
            {
                return this.employerState;
            }
            set
            {
                this.employerState = value;
            }
        }

        public string EmployerPostalCode
        {
            get
            {
                return this.employerPostalCode;
            }
            set
            {
                this.employerPostalCode = value;
            }
        }

        public string EmployerISOCountryCode
        {
            get
            {
                return this.employerISOCountryCode;
            }
            set
            {
                this.employerISOCountryCode = value;
            }
        }

        public string EmployerDisclaimer
        {
            get
            {
                return this.employerDisclaimer;
            }
            set
            {
                this.employerDisclaimer = value;
            }
        }
    }
}
