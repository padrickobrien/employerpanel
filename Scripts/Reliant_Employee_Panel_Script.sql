IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Custom_ReliantEmployee_Employer')
BEGIN
	DROP TABLE Custom_ReliantEmployee_Employer;
END
GO

IF EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Custom_ReliantEmployee')
BEGIN
	DROP TABLE [Custom_ReliantEmployee];
END
GO

CREATE TABLE [dbo].[Custom_ReliantEmployee]
(
	 [EmployeeID] INT NOT NULL
	,[SSN] VARCHAR(15)
	,[MatchedEmployee] VARCHAR(50)
	,[MatchedActiveRecord] VARCHAR(50)
	,[PayrollDisclaimer] VARCHAR(425)
	,[AlternateID] VARCHAR(50)
	,[EmployeeSSN] VARCHAR(15)
	,[FirstName] VARCHAR(50)
	,[LastName] VARCHAR(50)
	,[MiddleName] VARCHAR(50)
	,[PositionTitle] VARCHAR(75)
	,[DivisionCode] VARCHAR(50)
	,[StatusCode] VARCHAR(50)
	,[StatusMessage] VARCHAR(50)
	,[StatusType] VARCHAR(50)
	,[InfoEffectiveDate] DATE
	,[MostRecentHireDate] DATE
	,[TotalLengthOfSvc] INT
	,[TerminationDate] DATE
	,[ReferenceNumber] INT
	,[TrackingNumber] INT
	,[FraudAlert] VARCHAR(100)
	,[MilitaryAlert] VARCHAR(100)
	,[DisputeMessage] VARCHAR(100)
	,CONSTRAINT PK_EmployeeID PRIMARY KEY (EmployeeID)
	,CONSTRAINT FK_EmployeeID_DebtorID FOREIGN KEY (EmployeeID) REFERENCES Debtors(DebtorID),
);
GO

CREATE TABLE Custom_ReliantEmployee_Employer 
(
	 [EmployerID] int IDENTITY NOT NULL
	,[EmployeeID] int not null
	,[EmployerCode] VARCHAR(50)
	,[EmployerName] VARCHAR(50)
	,[EmployerStreet1] VARCHAR(50)
	,[EmployerStreet2] VARCHAR(50)
	,[EmployerCity] VARCHAR(50)
	,[EmployerState] VARCHAR(2)
	,[EmployerPostalCode] VARCHAR(5)
	,[EmployerISOCountryCode] VARCHAR(50)
	,[EmployerDisclaimer] VARCHAR(50)
	,CONSTRAINT PK_EmployerID PRIMARY KEY (EmployerID) 
	,CONSTRAINT FK_EmployeeID 
		FOREIGN KEY (EmployeeID) REFERENCES Custom_ReliantEmployee(EmployeeID)
		ON DELETE CASCADE --deletes child records when parent is deleted
);
GO

--Neat little trick I learned from StackOverflow: This ensures that at no point does this stored procedure not exist
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[Custom_AddEmployer]'))
   exec('CREATE PROCEDURE [dbo].[Custom_AddEmployer] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_AddEmployer]
(
	 @EmployeeID int
	,@EmployerCode varchar(50)
    ,@EmployerName varchar(50)
    ,@EmployerStreet1 varchar(50)
    ,@EmployerStreet2 varchar(50)
    ,@EmployerCity varchar(50)
    ,@EmployerState varchar(2)
    ,@EmployerPostalCode varchar(5)
    ,@EmployerISOCountryCode varchar(50)
    ,@EmployerDisclaimer varchar(50)
)
AS
BEGIN
SET NOCOUNT ON;
	INSERT INTO [Custom_ReliantEmployee_Employer]
	(
		 EmployeeID
		,EmployerCode
		,EmployerName
		,EmployerStreet1
		,EmployerStreet2
		,EmployerCity
		,EmployerState
		,EmployerPostalCode
		,EmployerISOCountryCode
		,EmployerDisclaimer
	)
	VALUES
	(
		 @EmployeeID
		,@EmployerCode
		,@EmployerName
		,@EmployerStreet1
		,@EmployerStreet2
		,@EmployerCity
		,@EmployerState
		,@EmployerPostalCode
		,@EmployerISOCountryCode
		,@EmployerDisclaimer
	);
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[Custom_UpdateEmployer]'))
   exec('CREATE PROCEDURE [dbo].[Custom_UpdateEmployer] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_UpdateEmployer]
(
	 @EmployeeID int
	,@EmployerCode varchar(50)
    ,@EmployerName varchar(50)
    ,@EmployerStreet1 varchar(50)
    ,@EmployerStreet2 varchar(50)
    ,@EmployerCity varchar(50)
    ,@EmployerState varchar(2)
    ,@EmployerPostalCode varchar(5)
    ,@EmployerISOCountryCode varchar(50)
    ,@EmployerDisclaimer varchar(50)
)
AS
BEGIN
SET NOCOUNT ON;
	UPDATE [dbo].[Custom_ReliantEmployee_Employer]
	SET 
		 [EmployerCode] = @EmployerCode
		,[EmployerName] = @EmployerName
		,[EmployerStreet1] = @EmployerStreet1
		,[EmployerStreet2] = @EmployerStreet2
		,[EmployerCity] = @EmployerCity
		,[EmployerState] = @EmployerState
		,[EmployerPostalCode] = @EmployerPostalCode
		,[EmployerISOCountryCode] = @EmployerISOCountryCode
		,[EmployerDisclaimer] = @EmployerDisclaimer
	WHERE [EmployeeID] = @EmployeeID;
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[Custom_AddEmployee]'))
   exec('CREATE PROCEDURE [dbo].[Custom_AddEmployee] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_AddEmployee] (
	 @EmployeeID int
    ,@MatchedEmployee varchar(50)
    ,@MatchedActiveRecord varchar(50)
    ,@PayrollDisclaimer varchar(425)
    ,@AlternateID varchar(50)
    ,@PositionTitle varchar(75)
    ,@DivisionCode varchar(50)
    ,@StatusCode varchar(50)
    ,@StatusMessage varchar(50)
    ,@StatusType varchar(50)
    ,@InfoEffectiveDate date
    ,@MostRecentHireDate date
    ,@TotalLengthOfSvc int
    ,@TerminationDate date
    ,@ReferenceNumber int
    ,@TrackingNumber int
    ,@FraudAlert varchar(100)
    ,@MilitaryAlert varchar(100)
    ,@DisputeMessage varchar(100)
)
AS
BEGIN
SET NOCOUNT ON;
	INSERT INTO [dbo].[Custom_ReliantEmployee]
	(
			[EmployeeID]
           ,[SSN]
           ,[MatchedEmployee]
           ,[MatchedActiveRecord]
           ,[PayrollDisclaimer]
           ,[AlternateID]
           ,[EmployeeSSN]
           ,[FirstName]
           ,[LastName]
           ,[MiddleName]
           ,[PositionTitle]
           ,[DivisionCode]
           ,[StatusCode]
           ,[StatusMessage]
           ,[StatusType]
           ,[InfoEffectiveDate]
           ,[MostRecentHireDate]
           ,[TotalLengthOfSvc]
           ,[TerminationDate]
           ,[ReferenceNumber]
           ,[TrackingNumber]
           ,[FraudAlert]
           ,[MilitaryAlert]
           ,[DisputeMessage])
	SELECT D.DEBTORID
		   ,D.SSN 
		   ,@MatchedEmployee
           ,@MatchedActiveRecord
           ,@PayrollDisclaimer
           ,@AlternateID
           ,D.SSN
           ,D.firstName
           ,D.lastName
           ,D.middleName
           ,@PositionTitle
           ,@DivisionCode
           ,@StatusCode
           ,@StatusMessage
           ,@StatusType
           ,@InfoEffectiveDate
           ,@MostRecentHireDate
           ,@TotalLengthOfSvc
		   ,@TerminationDate
           ,@ReferenceNumber
           ,@TrackingNumber
           ,@FraudAlert
           ,@MilitaryAlert
		   ,@DisputeMessage
		   FROM DEBTORS D
			WHERE D.DebtorID = @EmployeeID
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[Custom_GetEmployers]'))
   exec('CREATE PROCEDURE [dbo].[Custom_GetEmployers] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_GetEmployers] (@EmployeeID INT)
AS
BEGIN
SET NOCOUNT ON;
	SELECT 
		 [EmployerID]
		,[EmployeeID]
		,[EmployerCode]
		,[EmployerName]
		,[EmployerStreet1]
		,[EmployerStreet2]
		,[EmployerCity]
		,[EmployerState]
		,[EmployerPostalCode]
		,[EmployerISOCountryCode]
		,[EmployerDisclaimer]
	FROM [Custom_ReliantEmployee_Employer]
	WHERE [EmployeeID] = @EmployeeID;
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[Custom_UpdateEmployee]'))
   exec('CREATE PROCEDURE [dbo].[Custom_UpdateEmployee] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_UpdateEmployee] 
(
	 @EmployeeID int
	,@SSN varchar(15)
    ,@MatchedEmployee varchar(50)
    ,@MatchedActiveRecord varchar(50)
    ,@PayrollDisclaimer varchar(425)
    ,@AlternateID varchar(50)
    ,@EmployeeSSN varchar(9)
    ,@FirstName varchar(50)
    ,@LastName varchar(50)
    ,@MiddleName varchar(50)
    ,@PositionTitle varchar(75)
    ,@DivisionCode varchar(50)
    ,@StatusCode varchar(50)
    ,@StatusMessage varchar(50)
    ,@StatusType varchar(50)
    ,@InfoEffectiveDate date
    ,@MostRecentHireDate date
    ,@TotalLengthOfSvc int
    ,@TerminationDate date
    ,@ReferenceNumber int
    ,@TrackingNumber int
    ,@FraudAlert varchar(100)
    ,@MilitaryAlert varchar(100)
    ,@DisputeMessage varchar(100)
)
AS
BEGIN
SET NOCOUNT ON;
	UPDATE [dbo].[Custom_ReliantEmployee]
	SET 
	[SSN] = D.SSN
      ,[MatchedEmployee] = @MatchedEmployee
      ,[MatchedActiveRecord] = @MatchedActiveRecord
      ,[PayrollDisclaimer] = @PayrollDisclaimer
      ,[AlternateID] = @AlternateID
      ,[EmployeeSSN] = D.SSN
      ,[FirstName] = D.FirstName
      ,[LastName] = D.LastName
      ,[MiddleName] = D.MiddleName
	  ,[PositionTitle] = @PositionTitle
      ,[DivisionCode] = @DivisionCode
      ,[StatusCode] = @StatusCode
      ,[StatusMessage] = @StatusMessage
      ,[StatusType] = @StatusType
      ,[InfoEffectiveDate] = @InfoEffectiveDate
      ,[MostRecentHireDate] = @MostRecentHireDate
      ,[TotalLengthOfSvc] = @TotalLengthOfSvc
      ,[TerminationDate] = @TerminationDate
      ,[ReferenceNumber] = @ReferenceNumber
      ,[TrackingNumber] = @TrackingNumber
      ,[FraudAlert] = @FraudAlert
      ,[MilitaryAlert] = @MilitaryAlert
      ,[DisputeMessage] = @DisputeMessage
	  FROM Custom_ReliantEmployee CRE
	  INNER JOIN DEBTORS D ON D.DebtorID = CRE.EmployeeID 
	WHERE CRE.EmployeeID = @EmployeeID;
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Custom_GetEmployees'))
   exec('CREATE PROCEDURE [dbo].[Custom_GetEmployees] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_GetEmployees] (@FileNumber int)
AS
BEGIN
SET NOCOUNT ON;
	SELECT [EmployeeID]
      ,CRE.[SSN]
      ,[MatchedEmployee]
      ,[MatchedActiveRecord]
      ,[PayrollDisclaimer]
      ,[AlternateID]
      ,[EmployeeSSN]
      ,CRE.[FirstName]
      ,CRE.[LastName]
      ,CRE.[MiddleName]
      ,[PositionTitle]
      ,[DivisionCode]
      ,[StatusCode]
      ,[StatusMessage]
      ,[StatusType]
      ,[InfoEffectiveDate]
      ,[MostRecentHireDate]
      ,[TotalLengthOfSvc]
      ,[TerminationDate]
      ,[ReferenceNumber]
      ,[TrackingNumber]
      ,[FraudAlert]
      ,[MilitaryAlert]
      ,[DisputeMessage]
	FROM [dbo].[Custom_ReliantEmployee] CRE
	inner join Debtors D on CRE.EmployeeID = D.DebtorID
	WHERE D.Number = @FileNumber;
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[Custom_DeleteEmployee]'))
   exec('CREATE PROCEDURE [dbo].[Custom_DeleteEmployee] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_DeleteEmployee] (@EmployeeID INT)
AS
BEGIN
	DELETE FROM [Custom_ReliantEmployee]
	WHERE [EmployeeID] = @EmployeeID;
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[Custom_DeleteEmployer]'))
   exec('CREATE PROCEDURE [dbo].[Custom_DeleteEmployer] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[Custom_DeleteEmployer] (@EmployerID INT)
AS
BEGIN
	DELETE FROM [Custom_ReliantEmployee_Employer]
	WHERE [EmployerID] = @EmployerID;
END
GO

--Will need to be updated with the final name of the project
IF NOT EXISTS (SELECT TOP 1 * FROM WorkForm_Panels WHERE NAME = 'Employee Panel')
INSERT INTO WorkForm_Panels ([Name], [TypeName], [ControlData], [Enabled]) VALUES
(
	'Employee Panel',
	'EmployerPanelLib.EmployeePanel,EmployerPanelLib',
	NULL,
	1
)
GO


